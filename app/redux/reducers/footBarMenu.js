import { combineReducers } from 'redux'
import { Alert } from 'react-native'

const onChangeScreen = ( state, action ) =>{
	var prevState = Array.from(state);
	switch(action.type){
		case 'CHANGE_SCREEN': {
			prevState.location = action.screen;
			return {...prevState, lat: false, lon: false};
			break;
		}
		case 'DISPLAY_SUCURSAL_DETAILS' :{
			
			prevState.location = action.screen;
			return {...prevState, sucursal: action.sucursal, lat: false, lon: false}
			break;
		}
		case 'TRACE_ROUTE': {
			prevState.location = action.screen;
			return {...prevState, lat: action.lat, lon: action.lon}
			break;
		}
		default:{
			return state;
			break;
		}
	}

}


export default onChangeScreen;