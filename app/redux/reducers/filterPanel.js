import {Alert} from 'react-native'
const filterPanel = ( state = [], action  ) =>{

	switch(action.type){
		case 'CHANGE_VIEW_PANEL': {
			var prevState = Array.from(state);

			prevState.filterButtonPress = !action.filterButtonPress
			return {...action, filterButtonPress: prevState.filterButtonPress};
			break;
		}
		case "FILTERS" : {
			var filtros = false
			if( action.filters.c_fum || action.filters.c_wifi || action.filters.c_juntas || action.filters.c_drive ){
				//Alert.alert('EXISTE FILTROS', 'AUN HAY FILTROS DE SERVICIOS')
				filtros = true
			}
			
			return {filterButtonPress: true, ...action.filters, hasFilter: filtros }
		}

		case 'MODAL_QR':{

			return {...action};
		}
		default:{
			return state;
		}
	}

}

export default filterPanel;