import { createStore } from 'redux'
import onChangeScreen from '../reducers/footBarMenu'

const initialState = {
	screen: 'home',
	isModalVisible: false
}

const store = createStore(onChangeScreen, initialState);

export default store;  