import { createStore } from 'redux'
import filterPanel from '../reducers/filterPanel'

const storeFilter = createStore(filterPanel, { hasFilter: false, isModalVisible: false });

export default storeFilter;  