
import React, { Component } from 'react';

import {
  AppRegistry,
  StyleSheet,
  Alert
} from 'react-native';

import {
	View,
	Container,
	Content,
	Header,
	Body,
	Left,
	Right,
	Title,
	Icon,
	Text,
	Button,
	Footer,
	Center
} from 'native-base'

import Home 	from './screens/Home';
import MapView 	from 'react-native-maps';
import Api 		from './config/Api'
import FooterApp from './components/FooterApp';
import Promo 	from './screens/Promo'
import store from './redux/store/store'
import Sucursales from './screens/Sucursales'
import Detalle from './screens/Detalle'
import Login from './screens/Login'
export default class App extends Component{

  constructor(props){
    super(props)
    this.state = {
      location: 'login',
      enableScroll: true
    }
    store.subscribe(()=>{
    	this.setState({
    		location: store.getState().location
    	})
    })
  }

  _renderScreen(){
    let location = this.state.location
  	switch(location){
  		case 'home' :{
  			return (
          <Content style={{ backgroundColor: "#ffffff" }}>
            <Home />
          </Content>
        )
  			break;
  		}

  		case 'sucursales':{
        let lat = store.getState().lat;
        let lon = store.getState().lon;
  			return (
            <Body style={{ backgroundColor: "#ffffff" }}>
              <Sucursales lat={lat} lon={lon} />
            </Body>
          )
  			break;
  		}

  		case 'promociones':{
  			return (
            <Content style={{ backgroundColor: "#ffffff" }}>
              <Promo/>
            </Content>
          );
  			break;
  		}

      case 'detalles':{
        return (
            <Content style={{ backgroundColor: "#ffffff" }}>
              <Detalle sucursal={store.getState().sucursal}/>
            </Content>
          );
        break;
      }

      case 'login':{
        return (
          <Content style={{ backgroundColor: "#ffffff" }} >
            <Login/>
          </Content>
        );
        break;
      }
  	}
  }

  _renderFooter(){
      if(this.state.location != 'login'){
        return (
           <Footer style={{ backgroundColor: "#ffffff" }}>
              <FooterApp />
            </Footer>
        )
      }
  }

  render() {

    return (
        <Container style={{ backgroundColor: "#ffffff" }}>
          {this._renderScreen()}
          {this._renderFooter()}
        </Container>

    );
  }

}