import React, { Component } from 'react'

import FooterApp from '../components/FooterApp';
import { 
	Header, 
	Content, 
	Container, 
	Left, 
	Right, 
	Body, 
	Button, 
	Icon, 
	Title
} from 'native-base'

import { 
	Image,
	TouchableOpacity,
	TouchableHighlight,
	Text,
	View,
	Alert,
	AsyncStorage
} from 'react-native'

import{
	balanceTitle,
	balanceValue
}from '../config/styles'

import ModalQr from '../components/ModalQr';
import storeFilter from '../redux/store/storeFilter'

export default class Home extends Component{

	constructor(props){
		super(props);
		this.state = {
			isModalVisible: false,
			balance: "...",
			uid: '...'

		};

		storeFilter.subscribe( ()=>{
			this.setState({
				isModalVisible: storeFilter.getState().isModalVisible
			})
		})
	}

	_qrModalCode(){
		visible = this.state.isModalVisible;
		storeFilter.dispatch({ type: 'MODAL_QR', isModalVisible: !visible });
	}

	async _getSesion(){
		const sesion = await AsyncStorage.getItem('@session:user').then( (data) => data )
		let data = await JSON.parse(sesion)
		this.setState({
			uid: data.uid,
			balance: data.balance
		})
	}
	componentWillMount(){
		try{
			this._getSesion()
		}catch(error){
			Alert.alert('Error', ''+error)
		}
	}	

	render(){
		return(
			<View>
				<Text style={balanceTitle}>
					TU SALDO
				</Text>	
				<Text style={balanceValue}>
					{this.state.balance} puntos
				</Text>
				<TouchableHighlight onPress={()=>{this._qrModalCode()}} style={{ marginRight: 0,justifyContent: 'center', alignItems: 'center', }}>
					<Image
						source={require('../assets/img/cardASide/drawable-hdpi/card_a_side.png')}
						style={{
							flex: 1,
							width: 289,
							height: 389
						}}
					/>
				</TouchableHighlight>
				<View>
					<ModalQr uid={this.state.uid} />
				</View>
			</View>
		);
	}
}