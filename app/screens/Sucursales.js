import React, { Component } from 'react'

import {
	Text,
	Container,
	Content,
	Grid,
	Col,
	Row,
	Input
} from 'native-base'

import { 
	StyleSheet ,
	View,
	ScrollView,
	Dimensions,
	Alert,
	Image,
	TouchableOpacity,
} from 'react-native'

import MapView from 'react-native-maps';
import Api from '../config/Api'
import SearchBar from '../components/SearchBar'

import {
	resultBoxTitle
} from '../config/styles.js'

import storeFilter from '../redux/store/storeFilter'
import store from '../redux/store/store'

import Sucursal from '../components/Sucursal'
import {connect} from '../config/ServerConnect'
import Polyline from '@mapbox/polyline';

export default class Sucursales extends Component{

	constructor(props){
		super(props)
		this.state={
			currentRegion:{
				longitude: 0.00,
				latitude: 0.00,
				latitudeDelta: 0.00,
				longitudeDelta: 0.00,
			},
			sucursales: [],
			hasFilter: false,
			coords: [],
			finalLat: this.props.lat,
			finalLon: this.props.lon
		}

		store.subscribe(()=>{
			this.setState({
				finalLat: store.getState().lat,
				finalLon: store.getState().lon
			})
		})
		storeFilter.subscribe(()=>{
			this.setState({
				hasFilter: storeFilter.getState().hasFilter,
			})
		})
	}

	componentWillMount(){
		
	    navigator.geolocation.getCurrentPosition(
	      (position) => {
	       console.log(position);
	        this.setState({
	        	currentRegion: this.regionFrom(position.coords.latitude, 
	        									position.coords.longitude, 
	        									position.coords.accuracy) 
	        });

	       // Alert.alert("coords", "longitud: "+position.coords.longitude+' Longitud State: '+this.state.currentRegion.longitudeDelta+' latitude: '+position.coords.latitude+' accuracy: '+position.coords.accuracy)
	      },
	      (error) => alert(error.message),
	      {enableHighAccuracy: false, timeout: 40000, maximumAge: 20000}
	    );
		var sucursales = this._getSucursalesData();


		sucursales.then( response =>{
			let sucusalesArray = response.data.map( sucursal => sucursal );
			this.setState({
				sucursales: sucusalesArray,
			})
		})
	}

	loadTrace(initialPoints){
		let lat = this.state.finalLat;
		let lon = this.state.finalLon;
		if(lat !== false){
			this.getDirections(`${initialPoints.latitude},${initialPoints.longitude}`,`${lat},${lon}`);
			this.setState({
				finalLat: false,
				finalLon: false
			})
		}
	}

	regionFrom(lat, lon, accuracy) {
	    const oneDegreeOfLongitudeInMeters = 111.32 * 1000;
	    const circumference = (40075 / 360) * 1000;

	    const latDelta = accuracy * (1 / (Math.cos(lat) * circumference));
	    const lonDelta = (accuracy / oneDegreeOfLongitudeInMeters);
	    return {
	      latitude: lat,
	      longitude: lon,
	      latitudeDelta: Math.max(0, latDelta),
	      longitudeDelta: Math.max(0, lonDelta),
	    };
	}

	_getSucursalesData(){
		var api = new Api();
		var data = api.sendRequest();
		return data;

	}

	async getDirections(startLoc, destinationLoc) {
	   
	    try {
	    	let resp = await fetch(`https://maps.googleapis.com/maps/api/directions/json?origin=${startLoc}&destination=${destinationLoc}&key=${connect.google_key}`)
	        let respJson = await resp.json();
	      
	        let points = Polyline.decode(respJson.routes[0].overview_polyline.points);
	        let coords = points.map((point, index) => {
	        	
	            return  {
	                latitude : point[0],
	                longitude : point[1]
	            }
	        })
	        this.setState({coords: coords})
	        return coords
	    } catch(error) {
	        return error
	    }
	}

	render(){
		const { width, height } = Dimensions.get('window');
		const ratio = width / height;
		var hasFilter = this.state.hasFilter;
		var filtros = {
			c_fum: storeFilter.getState().c_fum,
			c_wifi: storeFilter.getState().c_wifi,
			c_juntas: storeFilter.getState().c_juntas,
			c_drive: storeFilter.getState().c_drive,
			filterText: storeFilter.getState().filterText
		}

		if(this.state.finalLat != false){
			let initialPoints = this.state.currentRegion;
			if(initialPoints.latitude != 0.00 && initialPoints.longitude != 0.00)
				this.loadTrace(initialPoints)
		}

		return (
			<View>
			<View style={{ width, height }}> 
				<MapView style={styles.map}
					region={this.state.currentRegion}
					initialRegion={this.state.currentRegion}
					loadingEnabled={true}
					minZoomLevel={2}
				>
					<MapView.Marker 
						coordinate={this.state.currentRegion}
						title={"Tu te encuentras aqui"}

					/>
					
					<MapView.Polyline
						coordinates={this.state.coords}
						strokeWidth={2}
						geodesic={true}
						strokeColor={"#ae0034"}
					/>
					{ 
						this.state.sucursales.map( (sucursal, i) => {
							if(hasFilter){
								if(filtros.c_fum && sucursal.c_fum == 1){
									return <MapView.Marker key={i} title={sucursal.nombre} description={sucursal.descripcion} coordinate={{ latitude: sucursal.lat, longitude: sucursal.lon }} /> 
									
								}

								if(filtros.c_drive && sucursal.c_drive == 1){
									return <MapView.Marker key={i} title={sucursal.nombre} description={sucursal.descripcion} coordinate={{ latitude: sucursal.lat, longitude: sucursal.lon }} /> 
									
								}

								if(filtros.c_juntas && sucursal.c_juntas == 1){
									return <MapView.Marker key={i} title={sucursal.nombre} description={sucursal.descripcion} coordinate={{ latitude: sucursal.lat, longitude: sucursal.lon }} /> 
									
								}
								if(filtros.c_wifi && sucursal.c_wifi == 1){
									return <MapView.Marker key={i} title={sucursal.nombre} description={sucursal.descripcion} coordinate={{ latitude: sucursal.lat, longitude: sucursal.lon }} /> 
										
									
								}

							}
							if(filtros.filterText != false && filtros.filterText != undefined){
								if(sucursal.nombre.indexOf(filtros.filterText) != -1){
									return <MapView.Marker key={i} title={sucursal.nombre} description={sucursal.descripcion} coordinate={{ latitude: sucursal.lat, longitude: sucursal.lon }} /> 
								}
							}
							else if(!hasFilter){
								return <MapView.Marker key={i} title={sucursal.nombre} description={sucursal.descripcion} coordinate={{ latitude: sucursal.lat, longitude: sucursal.lon }} /> 
							}
						})

					}

				</MapView>

				<SearchBar />
				<View>
					<View style={styles.sucursalesPanel}>
						<Text style={resultBoxTitle}>Cerca de mi</Text>
							<ScrollView>
								{
									this.state.sucursales.map( (sucursal, i) => {
										if(hasFilter){
											
											if(filtros.c_fum && sucursal.c_fum == 1 ){
												//Alert.alert('FILTRO', 'SUCURSALES -> HAY FILTRO')
												return <Sucursal key={i} sucursal={sucursal} lat1={this.state.currentRegion.latitude} lat2={sucursal.lat} lon1={this.state.currentRegion.longitude} lon2={sucursal.lon} /> 
												
											}
											
											if(filtros.c_drive && sucursal.c_drive == 1){
												
												return <Sucursal key={i} sucursal={sucursal} lat1={this.state.currentRegion.latitude} lat2={sucursal.lat} lon1={this.state.currentRegion.longitude} lon2={sucursal.lon} /> 
												
											}
											
											if(filtros.c_juntas && sucursal.c_juntas == 1){
												
												return <Sucursal key={i} sucursal={sucursal} lat1={this.state.currentRegion.latitude} lat2={sucursal.lat} lon1={this.state.currentRegion.longitude} lon2={sucursal.lon} /> 
												
											}
											if(filtros.c_wifi && sucursal.c_wifi ==  1){
												//Alert.alert('FILTRO', 'SUCURSALES2 -> HAY FILTRO')
												return <Sucursal key={i} sucursal={sucursal} lat1={this.state.currentRegion.latitude} lat2={sucursal.lat} lon1={this.state.currentRegion.longitude} lon2={sucursal.lon} /> 
												
											}

										}
										if( filtros.filterText != false && filtros.filterText != undefined){
											if(sucursal.nombre.indexOf(filtros.filterText) != -1){
												return <Sucursal key={i} sucursal={sucursal} lat1={this.state.currentRegion.latitude} lat2={sucursal.lat} lon1={this.state.currentRegion.longitude} lon2={sucursal.lon} /> 
											}
										
										}
										else if( !hasFilter){
											return <Sucursal key={i} sucursal={sucursal} lat1={this.state.currentRegion.latitude} lat2={sucursal.lat} lon1={this.state.currentRegion.longitude} lon2={sucursal.lon} /> 
										}
									})
								}
							</ScrollView>
					</View>
				</View>
			</View>
			</View>
		);
	}

}


const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    height: "100%",
    width: "100%",
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  sucursalesPanel: {
  	zIndex: 40,
  	borderTopLeftRadius: 10,
  	borderTopRightRadius: 10,
  	backgroundColor: "#ffff",
  	width: "100%",
  	height: 130,
  	position: "relative",
  	marginBottom: "23%"
  },
  searchBar: {
  	 backgroundColor: "#ffffff", 
  	 width: "89%" ,
  	 height: 50,
  	 borderRadius: 10,
  	 marginTop: 7
  }
});
