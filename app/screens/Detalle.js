import React, { Component } from 'react'
import {
	Container,
	Content,
	Row,
	Col,
} from 'native-base'

import {
	TouchableOpacity,
	Alert,
	StyleSheet,
	Text,
	Dimensions,
	View,
	Image
} from 'react-native'

import {colors} from '../config/styles'
import store from '../redux/store/store'

import Session from '../config/Session'
import ImageSlider from 'react-native-image-slider';

export default class Detalle extends Component{
	constructor(props){
		super(props);
	}

	_backPress(){
		store.dispatch({type: 'CHANGE_SCREEN', screen: 'sucursales'})
	}

	componentDidmount(){
		Session.getSession()
	}

	_traceRoute(){
		//Alert.alert('lon-lat', 'LAT -> '+this.props.sucursal.lat+' LON -> '+this.props.sucursal.lon)
		store.dispatch({ 
				type: 'TRACE_ROUTE', screen: 'sucursales', 
				lat: this.props.sucursal.lat, 
				lon: this.props.sucursal.lon 
		})
	}

	_renderStatus(is_open){
		if(is_open){
			return (<Col style={{ width: "47%" }}>
					<View style={{ flex: 1, alignItems: "center" }}>
						<View style={styles.openLabelBigOpen}>
							<Text style={styles.openLabelBigOpenText}>Abierto</Text>
						</View>
					</View>
				</Col>)

		}else{
			return (<Col style={{ width: "47%" }}>
					<View style={{ flex: 1, alignItems: "center" }}>
						<View style={styles.openLabelBigClose}>
							<Text style={styles.openLabelBigCloseText}>Cerrado</Text>
						</View>
					</View>
				</Col>)
		}
	}

	render(){
		return(
			<Container  style={{ paddingLeft: "3%", paddingRight: "3%" }}>
				<Content scrollEnabled={false}>
					<Row style={{ marginTop: "2.5%" }}>
						<Col style={{ width: "22%" }}>
							<TouchableOpacity onPress={ ()=>{this._backPress()} }>
								<Image
									source={require('../assets/img/backIcon/drawable-hdpi/arrow_back.png')}
									style={{ width: 20, height: 20 }}
								/>
							</TouchableOpacity>
						</Col>
						<Col>
							<Text style={styles.title}>{this.props.sucursal.nombre}</Text>
						</Col>
					</Row>
					<Row style={{ marginTop: 11 }}>
						<Container>
							<Content scrollEnabled={false} >
								<Row>
									<Col style={{marginLeft: -70}}>	
										<ImageSlider images={[
												this.props.sucursal.imagesFolder+'img1',
												this.props.sucursal.imagesFolder+'img2',
												this.props.sucursal.imagesFolder+'img3'
											]}
											style={{width: (Dimensions.get('window').width), height: (Dimensions.get('window').height * 0.5 )}}
										/>
									</Col>
								</Row>
								<Row style={{ marginTop: 8 }}>
									<Col style={{ width: "24.3%" }}>

									</Col>
									<Col style={{width: "40%"}}>
										<Text style={styles.descripcionLabel}>{this.props.sucursal.descripcion}</Text>
									</Col>
								</Row>
								<Row style={{ marginTop: 22 }}>
									<Col >
										<View style={styles.separatorLine}></View>
									</Col>
									<Col style={{ marginTop: -19, width: "20%" }}>
										<View style={{alignItems: 'center', justifyContent: "center" ,flexDirection: 'row', flex: 1}}>
										<Image
											source={require('../assets/img/flowerPinSmall/drawable-hdpi/flower_copy.png')}
											style={{width: 45, height: 45, alignItems: 'center', justifyContent: "center"}}
										/>
										</View>
									</Col>
									<Col style={{ width: "40%" }}>
										<View style={styles.separatorLine}></View>
									</Col>
								</Row>
								<Row>
									<Col style={{ width: "11.5%" }}></Col>
									<Col style={{ width: "80%" }}>
										<View style={{flex: 1, alignItems: "center", justifyContent: "center", flexDirection: "row"}}>
											<Text style={styles.addressLabel}>{this.props.sucursal.domicilio}</Text>
										</View>
									</Col>
								</Row>
								<Row style={{ marginTop: 15 }}>
									<Col style={{ width: "47%" }}>
										<Row>
											<View style={ { alignContent: "center", alignItems: "center", flex: 1 } }>
												<Text style={styles.scheduleTitle} >Horarios</Text>
											</View>
										</Row>
										<Row>
											<View>
												<Text style={styles.scheduleLabel}>{this.props.sucursal.horarios}</Text>
											</View>
										</Row>
									</Col>
									<Col style={{ width: "17%" }}></Col>
									<Col style={{ width: "42%" }}>
										<TouchableOpacity onPress={ ()=>{this._traceRoute() } } opacity={0.3} style={styles.goNowButtonBig}>
											<Row>
												<Image 
													source={require('../assets/img/goNowIconBig/drawable-hdpi/location.png')}
													style={{width: 35, height: 35}}
												/>
											</Row>
											<Row>
												<Text style={styles.irAhora}>Ir ahora</Text>
											</Row>
										</TouchableOpacity>
									</Col>
								</Row>
								<Row>
									{this._renderStatus(this.props.sucursal.is_open)}
								</Row>
							</Content>
						</Container>
					</Row>
				</Content>
			</Container>
		)
	}
}

const styles = {
	title : {
		fontFamily: 'Copperplate',
		fontSize: 24,
		color: colors.charcoal_grey,
	},
	descripcionLabel : {
		fontFamily: "TrebuchetMS",
		fontSize: 18,
		color: colors.charcoal_grey,
		textAlign: "center"
	},
	separatorLine : {
		width: 120,
		height: 2,
		borderStyle: "solid",
		borderWidth: 2,
		borderColor: colors.warm_grey
	},
	addressLabel: {
		fontFamily: "TrebuchetMS",
		fontSize: 14,
		fontStyle: "italic",
		textAlign: "center",
		color: colors.charcoal_grey
	},
	scheduleTitle : {
		width: 85,
		height: 18,
		fontFamily: "CopperplateBold",
		fontSize: 17,
		color: colors.charcoal_grey,
		textAlign: "center",
	},
	scheduleLabel: {
		fontSize: "TrebuchetMS",
		fontSize: 9,
		textAlign: "center",
		color: colors.charcoal_grey,
	},
	openLabelBigOpen: {
		width: 119,
		height: 25,
		borderRadius: 5,
		borderStyle: "solid",
		borderWidth: 1,
		borderColor: colors.pea_green,
		paddingTop: 2
	},
	openLabelBigClose: {
		width: 119,
		height: 25,
		borderRadius: 5,
		borderStyle: "solid",
		borderWidth: 1,
		borderColor: colors.lipstick,
		paddingTop: 2
	},

	openLabelBigOpenText: {
		color: colors.pea_green,
		textAlign: "center"
	},

	openLabelBigCloseText: {
		color: colors.lipstick,
		textAlign: "center"
	},
	goNowButtonBig:{
		width: 78,
		height: 78,
		borderRadius: 14,
		shadowOffset: {
			width: 0,
			height: 1
		},
		shadowRadius: 4,
		shadowOpacity: 1,
		shadowColor: "rgba(0,0,0 0.5)",
		backgroundColor: colors.cranberry,
		borderStyle: "solid",
		borderWidth: 1,
		borderColor: colors.cranberry,
		flex: 1,
		alignItems: "center",
		alignContent: "center",
		paddingTop: 11
	},
	irAhora:{
		fontSize: 14,
		fontFamily: "TrebuchetMS",
		color: "#ffffff",
		textAlign: "center"
	}
}