import React, { Component } from 'react'
import {
	Container,
	Content,
	Header,
	Left,
	Right,
	Text, 
	View,
	Spinner,
} from 'native-base'

import {
	ScrollView,
	Image,
	Alert,
	Dimensions
} from 'react-native'

import PromoCard from '../components/PromoCard'
import Api from '../config/Api'
import { promociones, promoHeader } from '../config/styles'
import store from '../redux/store/store';

export default class Promo extends Component{

	constructor(props){
		super(props);
		this.state = {
			data: [],
			load: true
		}
	}

	componentWillMount(){
		this.setState({
			load: true
		})
	}

	componentDidMount(){
		let api = new Api();
		let data = {};
		data = api.sendRequest('GET', 'get_promos');
		data.then(resp=>{
			const dato = resp.data.map( dato => dato );
			this.setState({
				data: dato,
				load: false,
			})
		})
	}

	componentWillUnmount(){
		if( this.state.load ){
			Alert.alert('Cargando informacion', 'Actualmente se estan cargando datos, ¿seguro que desea cambiar de ventana?', [
				{
					text: "Aceptar"
				},
				{
					text: "Cancelar",
					onPress: () =>{ store.dispatch({type: 'CHANGE_SCREEN', screen: 'promociones'}) }
				}
			])
		}
	}

	render(){
		const resizeMode = "stretch";
		return(
			<View>
	        	<Text style={promociones}>
	        		Promociones
	        	</Text>
	        	<View>
		        	<Image 
		        		source={require('../assets/img/promoHeader/drawable-hdpi/promociones_encabezado.png')}
		        		style={promoHeader}
		        	/>
	        	</View>

	        	<View >
	        		<View style={[styles.fixed, {marginTop: 100}]}>
						<Image
							resizeMode={'stretch'}
							source={require('../assets/img/loopBgLeft/background.png')}
							style={{
								width: Dimensions.get('window').width,
								resizeMode,
								height: Dimensions.get('window').width - 0.2,
							}}
						/>
					</View>
					<ScrollView style={[styles.fixed, {backgroundColor: 'transparent', marginBottom: 0}]}>
					{	
						( this.state.load ) ? <Spinner color='red' /> : this.state.data.map((dato, i) =>  <PromoCard num={i} key={i} data={dato} /> ) 
					}
					</ScrollView>
				</View>
			</View>	
		)
	}
}

const styles = {
	viewBackground: {
		position: "absolute",
		top: 0,
		left: 0,
		width: "100%",
		height: "100%",
	},
	fixed: {
	    position: 'absolute',
	    top: 0,
	    left: 0,
	    right: 0,
	    bottom: 0,
  },
  container: {
  		position: 'relative',
  },
}