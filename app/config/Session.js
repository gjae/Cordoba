import {
	AsyncStorage,
	Alert,
} from 'react-native'

import Api from './Api'
import store from '../redux/store/store'

export default class Session {

	static api(){
		return ( new Api() );
	}

	static async setSession(userData){
		try{
			AsyncStorage.setItem("@session:user", JSON.stringify(userData));
		}catch(error){
			Alert.alert("Error", 'Un error ha ocurrido al intentar guardar la sesion: '+error)
		}
	}

	static async destroy(){
		try{
			AsyncStorage.removeItem('@session:user');
		}catch(error){
			Alert.alert('Error', ''+error);
		}
	}

	static async startSession(num, password){


		let url = this.api().getUri()+'/auth'
		let req = await fetch(url,{
						method: "POST",
						headers: new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' }),
						body: "number="+num+"&password="+password
					})
		.then((resp)=>{
			return resp.json()
		})
		.then((data)=>{
			if( data.success ){
				let fecha = new Date();
				let user = data.data;
				user.dateTime = {
					time: fecha.getTime(),
					UTCHour: fecha.getUTCHours(),
					UTCDate: fecha.getUTCDate()
				}
				this.setSession(user);
				store.dispatch({type: "CHANGE_SCREEN", screen: 'home'})
				//Alert.alert("datos", ''+JSON.stringify(user))
			}
			
			else if(!data.success && data.error == "invalid number - password combination"){
				Alert.alert('Error', 'El numero y/o la contraseña no se encuentran registrados')
			}
		})
		return false;

	}

	static async getSession(){
		const sesion = await AsyncStorage.getItem('@session:user').then( (session)=> session )
		if(sesion !== null){
			var user = await JSON.parse(sesion)
			let time = new Date();
			time = time.getTime();
			let diff = (time - user.dateTime.time)/1000;
			diff /= 60;

			diff= Math.abs( Math.round(diff) )
			if( diff <= 45 ){
				time = new Date();
				user.dateTime.time = time.getTime()
				this.setSession(user);
				store.dispatch({type: "CHANGE_SCREEN", screen: "home"})
			}
			else{
				this.destroy()	
			}
			
		}
	}
}