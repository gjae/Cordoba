/**
 * CLASE CREADA PARA EL MANEJO DE TODO LO RELACIONADO AL SERVIDOR Y 
 * LA API
 */
import { connect } from './ServerConnect'

export default class Api{

	constructor(){
		this.connect = connect;
	}

	getHost(){
		return this.connect.host;
	}

	getProtocol(){
		return this.connect.protocol
	}

	getSecureProtocol(){
		return this.connect.secure_protocol
	}

	getApiServer(){
		return this.connect.api
	}

	/**
	 * OPTIENE EL RECURSO COMPLETO DE LA URI
	 * @param  {Boolean} secure SI SECURE ESTA EN FALSO ENTONCES RETORNA EL PROTOCOLO NO SEGURO (HTTP), 
	 *                          	DE CASO CONTRARIO RETORNA LA URI CON EL PROTOCOLO SEGURO (HTTPS)
	 * @return {String}         RETORNA LA URI COMPLETA A LA API
	 */
	getUri(secure = false){
		let uri = '';
		uri+= (secure) ? this.getSecureProtocol() : this.getProtocol();
		return uri+='//'+this.getHost()+'/'+this.getApiServer();
	}

	sendRequest(method = 'GET', side='get' ,accion='all', id=0 ,secure = false){

		let resource = ( accion == 'all' ) ? accion : accion+'='+id;
		let url =  this.getUri(secure)+'/'+side+'?'+resource;
		
		console.log(url)
		let data = {};
		data = fetch(url)
		.then( (response) => {
			return response.json()
		} )

		return data;
		//console.log(this.respuesta)

		//return url;
		
		//return url;
	}
}