const colors = {
	
	charcoal_grey: "#4a4a4a",
	faded_orange: "#eca145",
	cranberry: "#ae0034",
	pea_green: "#69b713",
	warm_grey: "#979797",
	lipstick: "#e41b30"
}

const container = {
	width: "100%",
	flex: 1,
	alignItems: 'flex-end',
	height: 62,
	justifyContent: "center",
	backgroundColor: colors.cranberry,

}

const footbar = {
	width: "100%",
	alignItems: 'flex-end',
	flex: 1,
	height: 67,
	backgroundColor: "transparent",
}


const footerbarTitle = {
	width: 80,
	height: 16,
	fontFamily: "TrebuchetMS",
	fontSize: 13,
	textAlign: "center",
	color: "#ffffff"
}

const footbarTitleActive = {
	width: 80,
	height: 16,
	fontFamily: "TrebuchetMS",
	fontSize: 13,
	textAlign: "center",
	color: colors.faded_orange
}

const homeIcon = {
	width: 36,
	height: 31,
}

const locationIcon = {
	width: 22,
	height: 31,
}

const promoIcon = {
	width: 41,
	height: 30,
}

const homeIconActive = {
	width: 36,
	height: 31,
}

const locationIconActive = {
	width: 22,
	height: 31,
}

const promoIconActive = {
	width: 41,
	height: 30,
	alignContent: "center"
}

const logo = {
	width: 53,
	height: 67,
}

const promoBox = {
	width: 353,
	height: 188,
	borderRadius: 7,
	shadowOffset: {
		width: 0,
		height: 2,
	},

	shadowRadius: 4,
	shadowOpacity: 1,
	shadowColor: "rgba(0,0,0, 0.5)",
	backgroundColor: "#ffffff",
	marginTop: 13,
	flex: 1
}

const promoTitle = {
	fontFamily: "CopperplateBold",
	fontSize: 18,
	//fontWeight: "bold",
	textAlign: "center",
	color: colors.charcoal_grey

}

const promoDescription = {
	fontFamily: "TrebuchetMS",
	fontSize: 12,
	textAlign: "justify",
	color: colors.charcoal_grey,
}

const vigenciaLabel = {
	width: 134,
	height: 13,
	maxHeight: 13,
	borderRadius: 6.5,
	backgroundColor: colors.faded_orange,
	textAlign: "center",
	alignItems: "center",
	marginTop: "auto",
	marginBottom: 20,
	flex: 1,
	top: 0
}

const vigenciaText = {
	fontSize: 8,
	marginLeft: 2.5
}

const vigenciaPromo = {
	fontSize: 11.5,
	marginLeft: 5.5,
	textAlign: "center"
}

const promociones = {
	//width: 159,
	//height: 24,
	fontFamily: "CopperplateBold",
	fontSize: 22,
	color: colors.charcoal_grey,
	textAlign: "center",
	backgroundColor: "#ffffff"
}

const promoHeader = {
	width: 306,
	height: 55
}

const balanceTitle = {
	//width: 73,
	//height: 14,
	fontFamily: "TrebuchetMS",
	fontWeight: "bold",
	textAlign: "center",
	color: "#e5a656",
	marginBottom: 9,
	marginTop: 5
}


const balanceValue = {
	fontFamily: "TrebuchetMS",
	fontSize: 28,
	fontWeight: "bold",
	textAlign: "center",
	color: colors.charcoal_grey,
	marginBottom: 15
}

const resultBoxTitle = {
	fontFamily: 'Copperplate',
	textAlign: "left",
	color: colors.charcoal_grey,
	marginTop: 9,
	fontSize: 24,
	marginLeft: 24,
	marginBottom: 12,
}

const distanceBox = {
	width: 61,
	height: 57,
	fontFamily: "CopperplateBold",
	backgroundColor: colors.faded_orange,
	borderStyle: "solid",
	borderWidth: 1,
	borderColor: colors.faded_orange,
	fontSize: 18,
	textAlign: "center",
	color: "#ffffff",
	marginLeft: 2,
	borderTopLeftRadius: 10,
	borderBottomLeftRadius: 10,
	paddingTop: 4,
	flex: 1,
	justifyContent: "center",
	alignItems: "center",
}

const resultAddress = {
	fontSize: 7, 
	fontFamily: "TrebuchetMS",
	color: colors.charcoal_grey
}

const resultTitle = {
	fontFamily: "TrebuchetMS",
	fontSize: 15,
	color: colors.charcoal_grey
}

const openLabel = {
	width: 29,
	height: 12,
	borderRadius: 2,
	borderStyle: "solid",
	borderWidth: 1,
	borderColor: colors.pea_green,
	fontFamily: "TrebuchetMS",
	fontSize: 6,
	textAlign: "center",
	color: colors.pea_green,
	paddingTop: 1

}
const closedLabel = {
	width: 29,
	height: 12,
	borderRadius: 2,
	borderStyle: "solid",
	borderWidth: 1,
	borderColor: colors.lipstick,
	fontFamily: "TrebuchetMS",
	fontSize: 6,
	textAlign: "center",
	color: colors.lipstick,
	paddingTop: 1

}

const categoryLabel = {
	width: 22,
	height: 12,
	fontFamily: "TrebuchetMS",
	fontSize: 5,
	color: colors.charcoal_grey,
	textAlign: "center"
}

const goNowButton = {
	width: 45,
	height: 45,
	borderRadius: 9,
	backgroundColor: colors.cranberry,
	borderStyle: "solid",
	borderWidth: 1,
	borderColor: colors.cranberry,
}

const filters = {
  	backgroundColor: "#ffffff", 
  	width: "89%" ,
  	height: 103,
  	borderRadius: 16,
  	marginTop: 7,
    shadowColor: 'red',
    shadowOffset: { height: 0, width: 0 },
    shadowOpacity: 0.75,
    shadowRadius: 5,
    elevation: 1,
}

const filterHeader = {
  	width: "100%",
  	height: 31,
  	borderRadius: 16,
  	borderBottomRightRadius: 0,
  	borderBottomLeftRadius: 0,
  	backgroundColor: colors.cranberry,
  	paddingLeft: 14,
  	paddingTop: 3
}

const filterTitle = {
	height: 18,
	width: 280,
	fontSize: 18,
	fontFamily: "CopperplateBold",
	textAlign: "left",
	color: "#ffffff",
}

const filterCategoryLabel = {
	fontFamily: "TrebuchetMS",
	fontSize: 12,
	textAlign:"center",
	color: colors.charcoal_grey,
}

const modalContainer = {
	marginLeft: 0, 
	marginRight: "3%", 
	marginTop: "2%" , 
	flexDirection: 'row',  
	flex: 1, 
	justifyContent: 'flex-end'

}

const modal = {
	backgroundColor: "#ffffff",
	height: "70%",
	borderRadius: 22,
	marginBottom:"19%",
	width: "100%",
	marginLeft: 0

}

const cardLabel = {
	width: 184,
	height: 72,
	fontFamily: "CopperplateBold",
	fontSize: 24,
	textAlign: "center",
	color: colors.charcoal_grey,
	justifyContent: "center",
	marginTop: "15%"
}

const footerButton = {
	height: "95%",
	flex: 1,
	flexDirection : "column",
	alignItems: "center",
	justifyContent: "center",

}

export {
	colors, 
	container, 
	footbar,
	footbarTitleActive,
	homeIcon,
	locationIcon,
	promoIcon,
	homeIconActive,
	promoIconActive,
	logo,
	footerbarTitle,
	promoBox,
	promoTitle,
	promoDescription,
	vigenciaLabel,
	vigenciaText,
	promociones,
	vigenciaPromo,
	promoHeader,
	balanceTitle,
	balanceValue,
	resultBoxTitle,
	distanceBox,
	resultAddress ,
	resultTitle,
	openLabel,
	closedLabel,
	categoryLabel ,
	goNowButton,
	filters,
	filterHeader,
	filterTitle,
	filterCategoryLabel,
	modal,
	modalContainer,
	cardLabel,
	footerButton,
}