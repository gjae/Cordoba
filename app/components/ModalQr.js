import React, { Component } from 'react'

import { 
	View,
	Text,
	Container,
	Content,
	Row,
	Col
} from 'native-base'

import {
	TouchableHighlight,
	Image,
	Alert
} from 'react-native'

import Modal from 'react-native-modal'
import storeFilter from '../redux/store/storeFilter'
import { modal, modalContainer, cardLabel } from '../config/styles'
import QRCode from 'react-native-qrcode';

export default class ModalQr extends Component{

	constructor(props){
		super(props);
		this.state = {
			isModalVisible: false,
			qr: null
		};

		storeFilter.subscribe( ()=>{
			this.setState({
				isModalVisible: storeFilter.getState().isModalVisible,
			});
		})
		
	}

	_showModal(){
		visible = this.state.isModalVisible;
		storeFilter.dispatch({ type: 'MODAL_QR', isModalVisible: !visible  });
	}

	componentDidMount(){
		this.setState({
			qr: <QRCode value={this.props.uid} size={210} />
		})
	}

	render(){
		var qr = this.state.qr;
		return(
			<Modal 
			    isVisible={this.state.isModalVisible}
			    style={modal}
			    onBackButtonPress={()=>{this._showModal()}}
			    onBackdropPress={()=>{this._showModal()}}
			    useNativeDriver={true}
			>
				<View style={{ flex: 1, height: "70%", }}>
			    	<Container>
			    		<Content scrollEnabled={false}>
			    			<Row>
			    				<Col style={{width: "100%"}}>
			    					<TouchableHighlight 
			    						style={modalContainer}
			    						onPress={()=>{this._showModal()}}
			    					>
			    						<Image
			    							source={require('../assets/img/closeIcon/drawable-hdpi/clear.png')}
			    							style={{ height: 26, width: 26 }}
			    						/>
			    					</TouchableHighlight>
			    				</Col>
			    			</Row>

			    			<Row>
			    				<Col style={{width: "100%"}}>
			    					<View style={{justifyContent: 'center', flexDirection: 'row', flex: 1}}>
							        	{qr}
							        </View>
			    				</Col>
			    			</Row>

			    			<Row>
			    				<Col style={{width: "100%"}}>
			    					<View style={{ justifyContent: 'center', flexDirection: 'row', flex: 1}}>
				    					<Text style={cardLabel}>
				    						Escanear Codigo 
				    					</Text>
			    					</View>
			    				</Col>
			    			</Row>
			    		</Content>
			    	</Container>
			    </View>
			</Modal>
		);
	}

}