import React, { Component } from 'react'

import{
	Container,
	Content,
	Body,
	Form,
	Input,
	Item,
	Label,
	Text,
	Button,
	Spinner,
} from 'native-base'

import {
	Image,
	View,
	Alert,
	AsyncStorage,
	Dimensions,
} from 'react-native'

import{ colors } from '../config/styles'
import Api from '../config/Api'

import store from '../redux/store/store'
import Session from '../config/Session'
export default class BoxLogin extends Component{

	constructor(props){
		super(props)
		this.state = {
			activoPassword: false,
			activoNum: false,
			password: null,
			num: null,
			inLogin: false,
			errorNum: false,
			errorPassword: false,
		}

		this.api = new Api();
	}

	_blurPassword(){
		let password = this.state.password;
		this.setState({
			activoPassword: (password != null && password!='') ? true : false
		})
	}

	_blurNum(){
		let num = this.state.num;
		this.setState({
			activoNum: ( num != null && num != '' ) ? true : false
		})
	}


	componentWillMount(){
		Session.getSession();
	}

	async _requestLoginHttp(){

		this.setState({
			inLogin: await Session.startSession(this.state.num, this.state.password),
		})
	}

	_sendLogin(){
		this.setState({
			inLogin: true
		})
		
		if( (this.state.password != null && this.state.password != '' ) && ( this.state.num != null && this.state.num != '' ) ){
			this.setState({
				inLogin: true,
				errorNum: false,
				errorPassword: false
			});
			this._requestLoginHttp();
		}else{
			if( this.state.num == null || this.state.num == '' )
				this.setState({ errorNum: true, inLogin: false })
			if( this.state.password == null || this.state.password == '' )
				this.setState({ errorPassword: true, inLogin: false })
		}
	}

	render(){
		const resizeMode = "cover";
		return (
			<Container>
				<Content style={{flex: 1}}>
					<View style={styles.viewBackground}>  
						<Image 
							resizeMode={'cover'}
							source={require('../assets/img/coffeeBg/drawable-hdpi/bitmap.png')}
							style={{
								width: Dimensions.get('window').width * 0.5,
								height: 539,
								flex: 1,
								resizeMode,
								position: "absolute"
							}}
						/>
					</View>
					<Content style={styles.loginContainer}>
						<View style={styles.viewLogin} >
							<Text style={styles.loginTitle}>
								Iniciar sesion
							</Text>
							<Form style={{width: "80%", marginLeft: "5%" }}>
								<Item floatingLabel error={this.state.errorNum}>
									<Label style={{ textAlign: (this.state.activoNum) ? 'left':'center', fontFamily: "TrebuchetMS", color: (this.state.activoNum)? "#eca145" : "#9b9b9b"  }}>No. de cuenta</Label>
									<Input onChangeText={(text)=>{ this.setState({num: text}) }} onBlur={()=>{ this._blurNum() }} onFocus={()=>{ this.setState({ activoNum: true }) }}/>
								</Item>
								<Item error={this.state.errorPassword} floatingLabel>
									<Label style={{ textAlign: (this.state.activoPassword) ? 'left':'center', fontFamily: "TrebuchetMS", color: (this.state.activoPassword)? "#eca145" : "#9b9b9b"  }}>Contraseña</Label>
									<Input onChangeText={(text)=>{ this.setState({password: text}) }} onBlur={()=>{ this._blurPassword() }} onFocus={()=>{ this.setState({ activoPassword: true }) }} ref={"labelPassoword"} secureTextEntry={true}  password={true}/>
								</Item>
							</Form>
						</View>
						<View style={{ flexDirection: "column", width: "100%", alignItems: "center", alignContent: "center", justifyContent: "center" }}>
							{
								this.state.inLogin ?  
								<Spinner color={'#ae0034'} />  :
									<Button onPress={ ()=>{this._sendLogin()} } disabled={this.state.inLogin}  block style={styles.loginButton}>
										{
											 <Text style={styles.loginButtonText}>Entrar</Text>
										}
										
									</Button>
							}
						</View>
						<View style={styles.brandContainer}>
							<Image
								source={require('../assets/img/flowerPin/drawable-hdpi/flower.png')}
								style={{
									width: 89,
									height: 89
								}}
							/>
							<Text>
								Cafe La Flor de Cordoba 
								<Image
									source={require('../assets/img/copyrightIcon/drawable-xxxhdpi/bitmap.png')}
									style={{
										width: 8,
										height: 8,
										marginLeft: 3
									}}
								/>
							</Text>
						</View>

					</Content>
				</Content>
			</Container>
		);
	}
}

const styles = {
	loginTitle:{
		width: 171,
		height: 24,
		fontFamily: "CopperplateBold",
		color: colors.charcoal_grey,
		marginLeft: 30,
		marginTop: 30,
	},
	loginContainer: {
	    width: "88%",
	    marginLeft: "6%",
	    marginTop: "20%",
	    border: 1,
	    borderStyle: "solid",
	    borderColor: "red"
	},
	viewBackground: {
		position: "absolute",
		top: 0,
		left: 0,
		width: "100%",
		height: "100%",
	},
	viewLogin: {
		height: 233,
	    backgroundColor: "#FFFFFF",
	    borderRadius: 20,
	    shadowOffset: {
	    	width: 0,
	    	height: 2
	    },
	    shadowRadius: 4,
	    shadowOpacity: 1,
	    shadowColor: "rgba(0, 0, 0, 0.5)",
	},
	loginButton: {
		width: 162,
		height: 57,
		backgroundColor: "#824009",
		borderRadius: 13,
		justifyContent: "center",
		alignItems: "center",
		alignContent: "center",
		flex: 1,
		flexDirection: "row",
		marginLeft: "22%",
		marginTop: "11%"
	},
	loginButtonText:{
		fontFamily: "TrebuchetMS",
		fontSize: 27,
		color: "#ffffff",
		textAlign: "center"
	},
	brandContainer:{
		width: "100%",
		alignContent: "center",
		alignItems: "center",
		justifyContent: "center",
		marginTop: 33
	},
	copy: {
		width: 178,
		height: 14,
		fontFamily: "Copperplate",
		fontSize: 14,
		color: colors.charcoal_grey
	}
}