import React, { Component } from 'react';
import { 
	StyleSheet ,
	View,
	Image,
	Alert,
	TouchableOpacity
} from 'react-native'

import {
	Text,
	Container,
	Content,
	Grid,
	Col,
	Row,
	Input
} from 'native-base'

import {
	filterHeader,
	filters,
	filterTitle,
	filterCategoryLabel
} from '../config/styles'

import storeFilter from '../redux/store/storeFilter'
export default class FilterPanel extends Component{
	constructor(props){
		super(props);
		this.state = {
			c_fum: false,
			c_drive: false,
			c_wifi: false,
			c_juntas: false,
			filterText: false
		}
		storeFilter.subscribe( ()=>{
			this.setState({
				filterButtonPress: storeFilter.getState().filterButtonPress,
				c_fum: storeFilter.getState().c_fum,
				c_drive: storeFilter.getState().c_drive,
				c_wifi: storeFilter.getState().c_wifi,
				c_juntas: storeFilter.getState().c_juntas,
				hasFilter: storeFilter.getState().hasFilter,
				filterText: storeFilter.getState().filterText
			})
		})
	}

	_closePanel(){
		var states = {
			...this.state
		}

		storeFilter.dispatch( {type: 'CHANGE_VIEW_PANEL', filterButtonPress: states.filterButtonPress, hasFilter: states.hasFilter, filterText: states.filterText } )
		
	}

	_filter(tipo){
		var states = {
			...this.state
		}
		switch(tipo){
			case 'c_fum':{
				states.c_fum = !states.c_fum;
				storeFilter.dispatch({type: "FILTERS",  filters: states })
				break;
			}
			case 'c_drive':{
				states.c_drive = !states.c_drive;
				storeFilter.dispatch({type: "FILTERS",  filters: states })
				break;
			}
			case 'c_juntas':{
				states.c_juntas = !states.c_juntas;
				storeFilter.dispatch({type: "FILTERS",  filters: states })
				break;
			}
			case 'c_wifi':{
				states.c_wifi =!states.c_wifi;
				storeFilter.dispatch({type: "FILTERS",  filters: states })
				break;
			}

		}
	}

	render(){
		var driveIcon = (this.state.c_drive) ?  require('../assets/img/driveIconActive/drawable-hdpi/directions_car_copy_2.png') : require('../assets/img/driveIcon/drawable-hdpi/directions_car_copy.png');
		var meetingsIcon = (this.state.c_juntas) ? require('../assets/img/meetingsIconActive/drawable-hdpi/177394_200_copy_3.png') :require('../assets/img/meetingsIcon/drawable-hdpi/177394_200_copy_2.png')
		var smokingIcon = (this.state.c_fum) ? require('../assets/img/smokingIconActive/drawable-hdpi/icon_048220_256_copy_2.png') : require('../assets/img/smokingIcon/drawable-hdpi/icon_048220_256_copy.png')
		var wifiIcon = (this.state.c_wifi) ? require('../assets/img/wifiIconActive/drawable-hdpi/signal_wifi_4_bar_copy_3.png') : require('../assets/img/wifiIcon/drawable-hdpi/signal_wifi_4_bar_copy_2.png')
		return(
			<View style={filters}>
				<Container>
					<Content>
						<Row style={filterHeader}>
							<Grid>
								<Col>
									<Text style={filterTitle} >Filtrar Sucursales</Text>
								</Col>
								<Col style={{ width: "80%" }}>
									
								</Col>
								<Col style={{ paddingTop: 5 }}>
									<TouchableOpacity onPress={ ()=>{ this._closePanel() } } >
										<Image
											source={require('../assets/img/closeFilterIcon/drawable-hdpi/clear_copy.png')} 
											style={{ width: 14, height: 13}}
										/>
									</TouchableOpacity>
								</Col>
							</Grid>
						</Row>
						<Row>
							<Grid style={{ paddingTop: 6 }}>
								<Col>
									<TouchableOpacity onPress={()=>{this._filter('c_drive')}} style={{ marginRight: 0,justifyContent: 'center', alignItems: 'center', }}>
										<Image 
											source={driveIcon}
											style={{ width: 20, height: 18 }}
										/>
										<Text style={filterCategoryLabel}>Drive Thru</Text>
									</TouchableOpacity>
								</Col>
								<Col>
									<TouchableOpacity onPress={()=>{this._filter('c_wifi')}} style={{ marginRight: 0,justifyContent: 'center', alignItems: 'center', }}>
										<Image 
											source={wifiIcon}
											style={{width: 22, height: 18}}
										/>
										<Text style={filterCategoryLabel}>WiFi</Text>
									</TouchableOpacity>
								</Col>
								<Col>
									<TouchableOpacity onPress={()=>{this._filter('c_fum')}} style={{ marginRight: 0,justifyContent: 'center', alignItems: 'center', }}>
										<Image 
											source={smokingIcon}
											style={{width: 16, height:18}}
										/>
										<Text style={filterCategoryLabel}>Area de fumadores</Text>
									</TouchableOpacity>
								</Col>
								<Col>
									<TouchableOpacity onPress={()=>{this._filter('c_juntas')}} style={{ marginRight: 0,justifyContent: 'center', alignItems: 'center', }}>
										<Image 
											source={meetingsIcon}
											style={{width: 23, height: 18}}
										/>
										<Text style={filterCategoryLabel}>{'Sala de\njuntas'}</Text>
									</TouchableOpacity>
								</Col>
							</Grid>
						</Row>
					</Content>
				</Container>
			</View>

		);
	}

}
