import React, { Component } from 'react'
import { 
	Container, 
	Header, 
	Content, 
	Footer, 
	FooterTab, 
	Button, 
	Text ,
	Icon,
	Grid,
	Col
} from 'native-base';
import {Image, Alert, View} from 'react-native';

import { 
	container, 
	footbar, 
	colors, 
	footbarTitleActive,
	footerbarTitle,
	homeIcon,
	locationIcon,
	promoIcon,
	logo

} from '../config/styles'

import onChangeScreen from '../redux/reducers/footBarMenu'
import store from '../redux/store/store'

export default class FooterApp extends Component{

	constructor(props){
		super(props);
		this.state = {
			location: 'home'
		}
	}

	_onChangeScreen(screen){
		this.setState({
			location: screen
		})
		store.dispatch( {type: 'CHANGE_SCREEN', screen: screen})
	}

	render(){

		return(
			<Container style={{ backgroundColor: "transparent" }}>
				<Footer style={footbar} >
					<FooterTab style={ container }>
						
						<Button onPress={ ()=>{ this._onChangeScreen('home') } }>
							{
								this.state.location== 'home' ?
									<Image
										style={homeIcon}
										source={require('../assets/img/homeIconActive/drawable-hdpi/shape_copy_3.png')}
									/>
								:
									<Image
										style={homeIcon}
										source={require('../assets/img/homeIcon/drawable-hdpi/shape_copy.png')}
									/>
							}
							{
								this.state.location == 'home' ?
									<Text style={footbarTitleActive}>
										Inicio
									</Text>
								:
									<Text style={footerbarTitle}>
										Inicio
									</Text> 
							}
						</Button>
										
						<Button onPress={ ()=>{ this._onChangeScreen('sucursales') } }>
							{
								this.state.location == 'sucursales' ?
									<Image
										style={locationIcon}
										source={require('../assets/img/locationIconActive/drawable-hdpi/shape_copy_4.png')}
									/>
								:
									<Image
										style={locationIcon}
										source={require('../assets/img/locationIcon/drawable-hdpi/shape_copy_2.png')}
									/>
							}
							{
								this.state.location == 'sucursales' ?
									<Text style={footbarTitleActive}>
										Sucursales
									</Text>								
								:
									<Text style={footerbarTitle}>
										Sucursales
									</Text>		
							}
						</Button>
								
						<Button  onPress={ ()=>{ this._onChangeScreen('promociones') } }>
							{
								this.state.location != 'promociones' ?
									<Image
										style={promoIcon}
										source={require('../assets/img/promoIcon/drawable-hdpi/694368_box_512_x_512_copy.png')}
									/>
								:
									<Image
										style={promoIcon}
										source={require('../assets/img/promoIconActive/drawable-hdpi/694368_box_512_x_512_copy_2.png')}
									/>

							}
							{
								this.state.location != 'promociones' ?
									<Text style={footerbarTitle}>
										Promociones
									</Text>
								:
									<Text style={footbarTitleActive}>
										Promociones
									</Text>
							}

						</Button>
						
					</FooterTab>
				</Footer>
			</Container>
		);

	}


}