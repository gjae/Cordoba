import React, { Component } from 'react'
import { 
	StyleSheet ,
	View,
	Image,
	Alert,
	TouchableOpacity,
	TextInput
} from 'react-native'

import {
	Text,
	Container,
	Content,
	Grid,
	Col,
	Row,
	Input
} from 'native-base'

import storeFilter from '../redux/store/storeFilter'
import FilterPanel from './FilterPanel'
export default class SearchBar extends Component{

	constructor(props){
		super(props)
		this.state = {
			filterButtonPress: false,
			hasFilter: false,
			c_fum: false,
			c_wifi: false,
			c_drive: false,
			c_juntas: false,
			filterText: false
		}
		storeFilter.subscribe(()=>{
			this.setState({
				filterButtonPress: storeFilter.getState().filterButtonPress,
				hasFilter: storeFilter.getState().hasFilter,
				c_fum: storeFilter.getState().c_fum,
				c_drive: storeFilter.getState().c_drive,
				c_wifi: storeFilter.getState().c_wifi,
				c_juntas: storeFilter.getState().c_juntas,
				filterText: storeFilter.getState().filterText,
			})
		})
	}

	_pressFilterButton(){
		var states = this.state

		storeFilter.dispatch({type: 'CHANGE_VIEW_PANEL', ...states })

	}

	_textFilter(target){
		//Alert.alert('EVENTO',target+' -> '+target.length)
		this.setState({
			filterText: (target.length > 0 ? target : false)
		})
		var states = { 
			...this.state
		}

		//Alert.alert('ahora', '-> '+(target.length > 0 ? 'HAY CARACTERES ':'NO HAY CARACTERES'))

		storeFilter.dispatch({type: 'FILTERS', filters:{...states, filterText: (target.length > 0 ? target : false) },  })
		
	}

	componentDidMount(){
		var states = { 
			...this.state
		}
		storeFilter.dispatch({type: 'FILTERS', filters:{...states, filterText: false },  })
	}

	_pressSearchButton(){
		this.refs.buscador._root.focus()
	}

	render(){
		var button = null;
		var filtros = null;
		if(!this.state.filterButtonPress){
			button = require('../assets/img/filterIcon/drawable-mdpi/group_3.png');
		}else{
			button = require('../assets/img/collapseIcon/drawable-mdpi/group_copy.png')
		}

		filtros = ( this.state.filterButtonPress ) ? <FilterPanel /> : null;

		return(
			<View style={{ flex: 1, width: "100%", alignItems: "center" }}>
				<View style={ styles.searchBar }>
					<Container>
						<Content>
							<Grid>
								<Col style={{ paddingTop: 8.5, paddingLeft: 3.7 }}>
									<TouchableOpacity onPress={()=>{ this._pressFilterButton() }}>
										<Image
											source={button}
										/>
									</TouchableOpacity>
								</Col>
								<Col style={{ width: "69%" }}>
									<Input ref={"buscador"} onChangeText={ (text)=>{ this._textFilter(text) } } />
								</Col>
								<Col style={{ paddingLeft: 3.7, paddingTop: 9.8 }}>
									<TouchableOpacity onPress={()=> {this._pressSearchButton()}}>
										<Image
											source={require('../assets/img/searchIcon/drawable-hdpi/shape.png')}
											style={{ flex:1 }}
										/>
									</TouchableOpacity>
								</Col>
							</Grid>
						</Content>
					</Container>
				</View>
				{filtros}
			</View>

		);
	}
}

const styles = StyleSheet.create({
  searchBar: {
  	 backgroundColor: "#ffffff", 
  	 width: "89%" ,
  	 height: 50,
  	 borderRadius: 10,
  	 marginTop: 7,
    shadowColor: 'red',
    shadowOffset: { height: 0, width: 0 },
    shadowOpacity: 0.75,
    shadowRadius: 5,
    elevation: 1,
  },

});
