import React, { Component } from 'react'

import {
	View,
	Text,
	Content,
	Body,
	Container
} from 'native-base'

import {
	Image,
	StyleSheet,
	Alert,
} from 'react-native'

import { Col, Row, Grid } from 'react-native-easy-grid';
import { 
	promoBox, 
	promoTitle, 
	promoDescription, 
	vigenciaLabel, 
	vigenciaText, 
	vigenciaPromo } from '../config/styles'
export default class PromoCard extends Component{

	constructor(props){
		super(props)
		this.state = {
			imagen : {
				uri: this.props.data.imageUrl
			}
		}
	}

	renderContentNormal(imagen){
		return (
			<Grid >
			    <Col>
			        <Text style={promoTitle}>
			           	{this.props.data.titulo}
			        </Text>
			        <Text style={promoDescription}>
			            {this.props.data.descripcion}
			        </Text>
			            <Body style={{ marginLeft: 12 }}>
			            	<Container>
			            		<Body style={vigenciaLabel}>
			            			<Grid>
			            				<Col>
			            					<Text style={vigenciaPromo}>
			            						{this.props.data.vigencia}
			            					</Text>
			            				</Col>
			            			</Grid>
			            		</Body>
			            	</Container>
			            </Body>
			    </Col>
			        <Col>
			           	<Image 
			            	source={{uri: imagen.uri}}
			            	style={{ width: 188, height: 188 }} 
			            />

			        </Col>
			</Grid>
		)
	}

	renderContentInverse(imagen){
		return (
			<Grid >
			    <Col>
			        <Image 
			            source={{uri: imagen.uri}}
			            style={{ width: 180 , height: 188 }} 
			       />

			    </Col>
			    <Col style={{ marginLeft: 12 }}>
			        <Text style={promoTitle}>
			           	{this.props.data.titulo}
			        </Text>
			        <Text style={promoDescription}>
			            {this.props.data.descripcion}
			        </Text>
			            <Body style={{ marginRight: 12 }}>
			            	<Container>
			            		<Body style={vigenciaLabel}>
			            			<Grid>
			            				<Col>
			            					<Text style={vigenciaPromo}>
			            						{this.props.data.vigencia}
			            					</Text>
			            				</Col>
			            			</Grid>
			            		</Body>
			            	</Container>
			            </Body>
			    </Col>
			</Grid>
		)

	}
	render(){
		var imagen = this.state.imagen
		//Alert.alert('IMAGEN',imagen.uri)
		//
		return(
			<View style={{ width: "90%" }}>
				<Container style={ promoBox }>
			        <Body >
			        	{ (this.props.num % 2 != 0) ? this.renderContentNormal(imagen) : this.renderContentInverse(imagen)}
			        </Body>
			     </Container>
			</View>
		);
	}

}