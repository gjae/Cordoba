import React, { Component } from 'react'

import {
	Container,
	Content,
	Grid,
	Col,
	Text,
	Row,
	View
} from 'native-base'

import {
	Image,
	TouchableHighlight,
	Alert,
	TouchableOpacity
} from 'react-native'

import {
	distanceBox,
	resultAddress ,
	resultTitle,
	openLabel,
	closedLabel,
	categoryLabel,
	goNowButton
} from '../config/styles'

import store from '../redux/store/store'
export default class Sucursal extends Component{

	constructor(props){
		super(props)
	}

	getDistanceFromLatLonInKm() {
	  var R = 6371; // Radius of the earth in km
	  var dLat = this.deg2rad(this.props.lat2-this.props.lat1);  // deg2rad below
	  var dLon = this.deg2rad(this.props.lon2-this.props.lon1); 
	  var a = 
	    Math.sin(dLat/2) * Math.sin(dLat/2) +
	    Math.cos(this.deg2rad(this.props.lat1)) * Math.cos(this.deg2rad(this.props.lat2)) * 
	    Math.sin(dLon/2) * Math.sin(dLon/2)
	    ; 
	  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
	  var d = R * c; // Distance in km
	  return d;
	}

	deg2rad(deg) {
	  return deg * (Math.PI/180)
	}
	_displayDetails(sucursal){
		store.dispatch({type: 'DISPLAY_SUCURSAL_DETAILS', screen: 'detalles', sucursal: sucursal});
	}

	_renderStatus(is_open){
		if(is_open){
			return 	(<Text style={openLabel}>
						Abierto
					</Text>)
		}
			return 	(<Text style={closedLabel}>
						Cerrado
					</Text>)

	}

	_traceRoute( lat , lon ){
		//Alert.alert('lon-lat', 'LAT -> '+this.props.sucursal.lat+' LON -> '+this.props.sucursal.lon)
		store.dispatch({ 
				type: 'TRACE_ROUTE', screen: 'sucursales', 
				lat: lat, 
				lon: lon
		})
	}

	render(){

		var wifiIcon = null;
		var juntasIcon = null;
		var fumaIcon = null;
		var driverIcon = null;
		if(this.props.sucursal.c_wifi == 1){
			wifiIcon = (
						<Col style={{ width: "5.8%", alignItems: "center" }}>
							<Image source={require('../assets/img/wifiResultIcon/drawable-hdpi/signal_wifi_4_bar_copy_4.png')}
							style ={{ width: 15, height: 12, }}/>
							<Text style={categoryLabel}>WiFi</Text>
						</Col>)
		}
		if( this.props.sucursal.c_juntas == 1){
			juntasIcon = (	
							<Col style={{ width: "5.8%", alignItems: "center" }}>
								<Image 
								source={require('../assets/img/meetingsResultIcon/drawable-hdpi/177394_200_copy_4.png')} 
								style={{ width: 15, height: 12 }}
								/>
								<Text style={categoryLabel}>{'Sala de\nJuntas'}</Text>
							</Col>
						)
		}

		if( this.props.sucursal.c_fum == 1){
			fumaIcon = (
				<Col style={{ width: "5.8%", alignItems: "center" }}>
					<Image
						source={require('../assets/img/smokingResultIcon/drawable-hdpi/icon_048220_256_copy_3.png')}
						style={{ width: 11, height: 12 }}
					/>
					<Text style={categoryLabel}>{'Fumador'}</Text>
				</Col>
			);
		}

		if( this.props.sucursal.c_drive == 1 ){
			driverIcon = (
				<Col style={{ width: "5.8%", alignItems: "center" }}>
					<Image
						source={require('../assets/img/driveResultIcon/drawable-hdpi/directions_car_copy_3.png')}
						style={{ width: 13, height: 12 }}
					/>	
					<Text style={categoryLabel}>Taxi</Text>
				</Col>
			);
		}
		return(
			
			<TouchableHighlight onPress={ ()=>{ this._displayDetails(this.props.sucursal) }}>
			<Row>
				<Grid>
					<Col style={{ paddingTop: 2, paddingLeft: 2, paddingRight: 2, width: 320 }}>
						<View>
							<Text style={distanceBox}>
								{ String( this.getDistanceFromLatLonInKm().toFixed(2) )+'\nKM'}
							</Text>
						</View>

					</Col>
					<Col style={{ marginLeft: -250, width: "80%" }}>
						<View>
							<Row>
								<Col style={{ width: "27%" }}>
									<Text style={resultTitle}>{this.props.sucursal.nombre}</Text>
								</Col>
								<Col>
									{this._renderStatus(this.props.sucursal.is_open)}
								</Col>
							</Row>
							<Row style={{ marginTop: -2 }}>
								<Col style={{ width: "34%", paddingTop: 2 }}>
									<Text style={resultAddress } >{this.props.sucursal.descripcion}</Text>
								</Col>
								{wifiIcon}
								{juntasIcon}
								{fumaIcon}
								{driverIcon}
							</Row>
						</View>
					</Col>
					<Col style={{ marginLeft: "-20%" }}>
						<TouchableOpacity onPress={()=>{ this._traceRoute(this.props.sucursal.lat,  this.props.sucursal.lon) }} opacity={0.3} style={[goNowButton, {alignContent: 'center',alignItems:"center"}]}>
							<Row>
								<Image 
									source={require('../assets/img/goNowIconBig/drawable-hdpi/location.png')}
									style={{width: 20, height: 20, marginTop: 4}}
								/>
							</Row>
							<Row>
								<Text style={styles.irAhora}>Ir ahora</Text>
							</Row>
						</TouchableOpacity>
					</Col>
				</Grid>
			</Row>
			</TouchableHighlight>
		)

	}
}

const styles = {
	irAhora:{
		fontSize: 9,
		fontFamily: "TrebuchetMS",
		color: "#ffffff",
		textAlign: "center"
	}
}